A simple redis check solution.

```
yarn install
```

Dev task:

```
yarn dev
```

Server will be available under http://localhost:8888

Please, create folders for md and pdf in the root directory:
```
md
pdf
```

Make sure to run two instances of redis: under 6789 and 6000 ports