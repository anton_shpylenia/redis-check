const express = require('express');
const { runCheck } = require('./app.js');
const bodyParser = require('body-parser');
var fs = require('fs');

const app = express();

app.set('view engine', 'pug');
app.set('views', './views');
app.use(bodyParser.urlencoded({
  extended: true
}));

app.get('/', function (req, res) {

  var files = fs.readdirSync(__dirname + '/pdf/');

  res.render('index', { files })
});
app.use('/pdf', express.static(__dirname + '/pdf'));

app.post('/report', async function (req, res) {
  console.log(req.body.key);
  try {
    const r = await runCheck(req.body.key, req.body.exclude)
    if (r) {
      res.redirect(`pdf/${r}`);
    } else {
      res.send('No errors');
    }
  } catch (e) {
    console.log(e);
    res.status(500).send(JSON.stringify(e));
    // res.render('index', {error: e});
  }
});
app.get('/report', async function (req, res) {
  console.log(req.query.key);
  try {
    const r = await runCheck(req.query.key)
    res.redirect(`pdf/${r}`);
  } catch (e) {
    console.log(e);
    res.status(500).send(JSON.stringify(e));
    // res.render('index', {error: e});
  }
});


export default app;
