require('colors');
import redis from 'redis';
import fs from 'fs';
import path from 'path';
import { promisifyAll } from 'bluebird';

const diff = require("deep-object-diff").diff;
const u = new Set();
var markdownpdf = require("markdown-pdf");
var diff2 = require('diff-json-structure');

promisifyAll(redis.RedisClient.prototype);
promisifyAll(redis.Multi.prototype);

export async function runCheck(key = '*content-coba-cache-%2*web*country%3dgb*', exclude = '') {
  const reportName = escape(`${key}-${exclude}`);
  const sourceClient = redis.createClient(6000, { detect_buffers: true });
  const newClient = redis.createClient({ detect_buffers: true });

  sourceClient.on("error", function (err) {
    throw err;
  });

  newClient.on("error", function (err) {
    throw err;
  });
  // const pattern = '*content-coba-cache-%2*mobile*%3fcountry%3dde*';
  const pattern = key;
  const keysSource = await sourceClient.keysAsync(pattern).then((res) => {
    return res;
  }).catch((e) => {
    throw e;
  });
  const keysNew = await sourceClient.keysAsync(pattern).then((res) => {
    return res;
  }).catch((e) => {
    throw e;
  });
  const logStream = fs.createWriteStream(path.join(
    __dirname, `/md/${reportName}.md`));
  console.log('Source keys: %s, New keys: %d', keysSource.length, keysNew.length);

  const errors = [];
  let i = 0;

  const length = Object.keys(keysSource).length;
  console.log(length);
  const excludedKeys = exclude.split(',');
  console.log(excludedKeys);
  const filteredKeys = keysSource.filter((key) => {
    return !excludedKeys.some((exKey) => {
      // console.log(exKey, key);
      return key.includes(exKey)
    });

  });

  console.log(filteredKeys.length);
  try {
    for (let key of filteredKeys) {
      const sourceResult = await sourceClient.getAsync(key);
      let sourceData = {};
      let newResult = {};
      let newData = {};
      if(!sourceResult) return;
      sourceData = JSON.parse(new Buffer(JSON.parse(sourceResult).Data, 'base64'));
      newResult = await newClient.getAsync(key);
      const _newData = JSON.parse(newResult);
      if (_newData && _newData.Data) {
        newData = JSON.parse(new Buffer(_newData.Data, 'base64'));
      }

      // console.log(sourceData.nodeId);
      if (sourceData && newData) {
        delete sourceData['nodeId'];
        delete newData['nodeId'];

        // console.log(sourceData.nodeId);
        i++;
        // console.log(key);
        const sourceString = JSON.stringify(sourceData);
        const newString = JSON.stringify(newData).replace(/alpha\./g, '');
        if (sourceString !== newString) {
          const source1 = JSON.parse(sourceString);
          const new1 = JSON.parse(newString);
          const diffs = diff2(source1, new1);
          let alternativeDiffs = diff(source1, new1);

          if(JSON.stringify(alternativeDiffs).includes('editUrl')) {
            alternativeDiffs = {};
          }
          // deepLog(key, source1, new1)



          // if (key.includes('fweb%2fpage%2fregistration%3fcountry%3dpt%26language%3den%26page%3dviaverde')
          //     // && !key.includes('page%2Ftip')
          //     // && !key.includes('page%2Flp')
          //   ) {
            errors.push({
              key,
              diffs,
              alternativeDiffs,
            });
          // }



        }
      }
    }
  } catch (e) {
    console.error('smthng ', e)
  }
  console.log('Total %s, Errors %d', keysSource.length, errors.length);

  if (errors.length > 0) {
    // logStream.write(errors.map(el => JSON.stringify(el)).join('\n'));
    // logStream.write(errors.map(el => {
    //     return `${el.key}${'\n```json\n' + JSON.stringify(el.diffs) + '\n' + JSON.stringify(el.deepDiffs) + '\n```'}`
    //   }).join('\n')
    // );

    const values = prettify(errors);
    // Object.keys(values).forEach(key => {
    //   logStream.write(
    //     `${key}
    //
    //     ${values[key].map(el => {
    //       return `${el.key}${'\n```json\n' + JSON.stringify(el.diffs) + '\n' + JSON.stringify(el.deepDiffs) + '\n```'}`
    //     }).join('\n')}
    //     `
    //   );
    // });

    logStream.write(`# Report for ${reportName} #\n`);
    logStream.write(`# Excluded keywords ${exclude} #\n`);
    logStream.write(`Total pages: ${Object.keys(values).length}\n`);
    logStream.write(`Total errors: ${errors.length}\n`);
    logStream.write(
      Object.keys(values).map(key => {
        // return `<details><summary> ${key} </summary>` + '\n' + '<p>\n' +

        return `# ${key} #\n\n` +
          values[key].map(el => {
          return '- ' + `${'https://beta.content-api.drive-now.com' + unescape(el.key.replace('content-coba-cache-', ''))}` + '\n\nType diff\n```diff\n' + printDiff(el.diffs) + '\n```' + '\nContent diff\n```diff\n'+ JSON.stringify(el.alternativeDiffs, null, 2) + '\n```\n'
        }).join('\n\n');
        //+ `</p>\n</details>`
      }).join('\n\n')
    );

    console.log(`Errors are saved to ${reportName}.md`);



    const pdf = await new Promise((res, rej) => {
      fs.createReadStream(path.join(__dirname,`/md/${reportName}.md`))
      .pipe(markdownpdf())
      .pipe(fs.createWriteStream(path.join(__dirname,`/pdf/${reportName}.pdf`)))
      .on('finish', () => {
        res(`${reportName}.pdf`)
      });
    });

    return pdf;
  }

  return null;

  console.log(u);
  sourceClient.quit();
  newClient.quit();
}

export function printDiff(parts) {
  var string = '';
  parts.forEach(function (part, i) {
    part.value
      .split('\n')
      .filter(function (line) { return !!line; })
      .forEach(function (line) {
        // if(line.includes(`"text":	"<string>"`) || line.includes(`"value":	"<string>"`)) {
        //   return;
        // }
        if (part.added) {
          string = string + '+  ' + line + '\n';
        } else if (part.removed) {
          string = string + '-  ' + line + '\n';
        } else {
          const siblingParts = parts.slice(i > 3 ? i - 3 : 0, i + 3);
          if(siblingParts.every(part => part.removed || part.added)) {
            string = string + '   ' + line + '\n';
          }
        }
      });
  });

  return string;
}

function prettify(logs) {
  const pages = logs.reduce((accum, el) => {
    const key = el.key;
    const page = parseKey(key);
    // if(page.includes('registration') || page.includes('tips') || page.includes('tip') || page.includes('lp')) {
    //   return accum;
    // }
    if(!accum.hasOwnProperty(page)) {
      accum[page] = [el]
    } else {
      accum[page].push(el);
    }
    return accum;
  }, {});
  return pages;
}

function parseKey(key, pattern = 'page%2f') {
  var startIndex = key.indexOf(pattern);
  var end = key.substring(startIndex, key.length);
  var page = end.substring(pattern.length, end.indexOf('%3f'));

  return page;
}

function deepLog(content, source1, new1) {
  if (source1 && new1) {
    Object.keys(source1).forEach((key) => {
      const difference = diff(source1[key], new1[key]);
      // if (difference && Object.keys(difference).length > 1) {
      if (JSON.stringify(source1[key]) !== JSON.stringify(new1[key])) {
        console.log(unescape(content), key, difference);
        checkUnique(content);

        if (source1[key] && typeof source1[key] === 'object' && !Array.isArray(source1[key]) &&
          new1[key] && typeof new1[key] === 'object' && !Array.isArray(new1[key]
          )
        ) {
          deepLog(content, source1[key], new1[key]);
        }
      }
    });
  }
}

function checkUnique(s) {
  const r = s.replace(/content-coba-cache-%2Fweb%2Fpage%2F(.*.)%3F.*/, '$1');
  u[r] = true;
}


export async function mobileKeys() {
  const logStream = fs.createWriteStream(path.join(
    __dirname, 'error2.log'));
  const newClient = redis.createClient({ detect_buffers: true });
  const pattern = '*content-coba-cache-%2Fmobile*';
  const keysNew = await newClient.keysAsync(pattern).then((res) => {
    return res.map((el) => {
      return 'https://beta.content-api.drive-now.com' + unescape(el.replace('content-coba-cache-', ''));
    });
  });
  logStream.write(keysNew.join('\n'));
  newClient.quit();
}

export async function prc() {
  const logStream = fs.createWriteStream(path.join(
    __dirname, 'prc.log'));
  const newClient = redis.createClient({ detect_buffers: true });
  const pattern = '*promocode*';
  const keysNew = await newClient.keysAsync(pattern).then((res) => {
    return res.map((el) => {
      return unescape(el.replace('content-coba-cache-', ''));
    });
  });
  logStream.write(keysNew.join('\n'))
  // console.log(keysNew);
  newClient.quit();
}

export default runCheck;