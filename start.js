require('babel-core/register');
require("babel-polyfill");


const server = require('./main.js').default;

const port = process.env.PORT || 8888;
server.listen(port, '0.0.0.0');
console.log(`Application listening on port ${port}`);
process.on('uncaughtException', (err) => {
  console.log(err);
});

module.exports = server;
